(function ($) {

  Drupal.behaviors.tinkoff_credit = {
    attach: function (context, settings) {

      $('#buy-in-credit-button', context).click(function (event) {
        var product_id = $(this).data('product-id');
        $.ajax({
          method: "POST",
          url: "/tinkoff-credit-callback/" + product_id,
          data: {product_id: product_id},
          success: function (result) {
            $("#tinkoff-credit-wrapper").html(result);
          }
        })
      });

      // Отправка формы из карточки товара
      $(document).ajaxComplete(function (event, xhr, settings) {
        var creditForm = $('#tinkoff-credit-form');
        if (creditForm.length > 0) {
          creditForm.submit().remove();
        }
      });

      $('#views-form-commerce-cart-form-default', context).ajaxComplete(function
          (event, xhr, settings) {
        $('#tinkoff-credit-form').submit().remove();
      });
    }
  };

})(jQuery);
