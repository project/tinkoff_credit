<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 28/03/2018
 * Time: 19:37
 */

function tinkoff_credit_settings_form($form, &$form_state) {
  $mode = variable_get('tinkoff_credit_mode', 0);
  $form['mode'] = array(
    '#type' => 'radios',
    '#options' => array(
      0 => t('Test mode'),
      1 => t('Production mode')
    ),
    '#default_value' => $mode,
  );
  $form['groups'] = array('#type' => 'vertical_tabs');
  $form['test_mode'] = array(
    '#type' => 'fieldset',
    '#title' => t('Test settings'),
    '#group' => 'groups',
  );
  $form['test_mode']['test_remote_url'] = array(
    '#title' => t('Test url'),
    '#type' => 'textfield',
    '#default_value' => variable_get('tinkoff_credit_test_remote_url', ''),
    '#disabled' => TRUE,
  );
  $form['test_mode']['test_shop_id'] = array(
    '#title' => t('Test shop id'),
    '#type' => 'textfield',
    '#default_value' => variable_get('tinkoff_credit_test_shop_id', ''),
    '#disabled' => TRUE,
  );
  $form['production_mode'] = array(
    '#type' => 'fieldset',
    '#title' => t('Production settings'),
    '#group' => 'groups',
  );
  $form['production_mode']['production_remote_url'] = array(
    '#title' => t('Production url'),
    '#type' => 'textfield',
    '#default_value' => variable_get('tinkoff_credit_production_remote_url', ''),
  );
  $form['production_mode']['production_shop_id'] = array(
    '#title' => t('Production shop id'),
    '#type' => 'textfield',
    '#default_value' => variable_get('tinkoff_credit_production_shop_id', ''),
  );
  if ($mode == 1) {
    $form['production_mode']['production_shop_id']['#disabled'] = TRUE;
    $form['production_mode']['production_remote_url']['#disabled'] = TRUE;
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );
  return $form;
}


function tinkoff_credit_settings_form_submit($form, &$form_state) {
  variable_set('tinkoff_credit_production_remote_url', $form_state['values']['production_remote_url']);
  variable_set('tinkoff_credit_production_shop_id', $form_state['values']['production_shop_id']);
  if (empty($form_state['values']['production_shop_id'])) {
    variable_set('tinkoff_credit_mode', 0);
  }
  if (isset($form_state['values']['mode'])) {
    variable_set('tinkoff_credit_mode', (int) $form_state['values']['mode']);
  }
}

function tinkoff_credit_promocode_page($id = NULL) {
  if (empty($id)) {
    drupal_set_title(t('Add promocode'));
    $promocode = (object) array(
      'id' => NULL,
      'type' => 0,
      'promocode_name' => '',
    );
  }
  else {
    $promocode = tinkoff_credit_get_promocode_by_id($id);
  }
  return drupal_get_form('tinkoff_credit_promocode_form', $promocode);
}


function tinkoff_credit_promocode_form($form, &$form_state, $promocode) {
  $form['#tree'] = TRUE;
  $form['#promocode'] = $promocode;
  $form['promocode_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Promocode'),
    '#default_value' => $promocode->promocode_name,
    '#required' => TRUE,
  );
  $form['button_type'] = array(
    '#type' => 'select',
    '#options' => tinkoff_credit_button_types(),
    '#default_value' => $promocode->type,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  $form['cancel'] = array(
    '#type' => 'markup',
    '#markup' => l(t('Cancel'), 'admin/commerce/config/tinkoff-credit/promocodes'),
  );
  return $form;
}


function tinkoff_credit_promocode_form_validate($form, &$form_state) {
  if (empty($form_state['values']['promocode_name'])) {
    form_set_error('promocode_name');
  }
  if (!isset($form_state['values']['button_type'])) {
    form_set_error('button_type');
  }
}

function tinkoff_credit_promocode_form_submit($form, &$form_state) {
  $update = array();
  $record = new stdClass();
  $record->type = $form_state['values']['button_type'];
  $record->promocode_name = $form_state['values']['promocode_name'];
  if (!empty($form['#promocode']->id)) {
    $record->id = $form['#promocode']->id;
    $update = array('id');
  }
  drupal_write_record('tinkoff_credit_promocodes', $record, $update);
  $form_state['redirect'] = 'admin/commerce/config/tinkoff-credit/promocodes';
}

function tinkoff_credit_promocode_delete($form, &$form_state, $id) {
  $form['promocode_id'] = array(
    '#type' => 'value',
    '#value' => $id,
  );
  foreach (field_info_field_map() as $field_name => $field_stub) {
    if ($field_stub['type'] == 'tinkoff_credit_promocodes_list') {
      $query = new EntityFieldQuery();
      $query->fieldCondition($field_name, 'value', $id, '=');
      $result = $query->execute();
      if (!empty($result)) {
        $form['submit'] = array(
          '#type' => 'submit',
          '#value' => t('Delete'),
          '#disabled' => TRUE,
        );
        $form['cancel'] = array(
          '#type' => 'markup',
          '#markup' => l(t('Cancel'), 'admin/commerce/config/tinkoff-credit/promocodes'),
        );
        $form['description']['#markup'] = '<p>' . t('Promocode already in use') . '</p>';
        return $form;
      }
    }
  }
  return confirm_form($form, t('Are you sure you want to delete promocode %id', array('%id' => $id)), 'admin/commerce/config/tinkoff-credit/promocodes', t('This action cannot be undone.'), t('Delete'), t('Cancel'));
}

function tinkoff_credit_promocode_delete_submit($form, &$form_state) {
  $promocode_id = $form_state['values']['promocode_id'];
  db_delete('tinkoff_credit_promocodes')
    ->condition('id', $promocode_id)
    ->execute();
  watchdog('tinkoff_credit', 'Deleted promocode %id', array('%id' => $promocode_id));
  drupal_set_message(t('The promocode %id was deleted.', array('%id' => $promocode_id)));
  $form_state['redirect'] = 'admin/commerce/config/tinkoff-credit/promocodes';
}


function tinkoff_credit_promocodes_list() {
  $page = array();
  $promocodes = tinkoff_credit_get_promocodes();
  $page['promocodes_list'] = array(
    '#markup' => theme('tinkoff_credit_promocodes_list', array('promocodes' => $promocodes)),
  );
  return $page;
}


function theme_tinkoff_credit_promocodes_list($variables) {
  $header = array(
    t('Promocode'),
    t('Type'),
    array('data' => t('Operations'), 'colspan' => 3),
  );
  $promocodes = array();
  if (!empty($variables['promocodes'])) {
    $promocodes = $variables['promocodes'];
  }
  $rows = array();
  $button_types = tinkoff_credit_button_types();
  foreach ($promocodes as $id => $item) {
    $row = array();
    $row[] = l($item['name'], 'admin/commerce/config/tinkoff-credit/promocodes/' . $id . '/edit');
    $row[] = t($button_types[$item['type']]);
    $row[] = l(t('edit'), 'admin/commerce/config/tinkoff-credit/promocodes/' . $id . '/edit');
    $row[] = l(t('delete'), 'admin/commerce/config/tinkoff-credit/promocodes/' . $id . '/delete');
    $row[] = '';
    $rows[] = $row;
  }
  if (empty($rows)) {
    $rows[] = array(
      array(
        'colspan' => 4,
        'data' => t('There are currently no promocodes. <a href="!url">Add a new promocode</a>.', array('!url' => url('admin/commerce/config/tinkoff-credit/promocodes/add'))),
      ),
    );
  }
  return theme('table', array('header' => $header, 'rows' => $rows));
}