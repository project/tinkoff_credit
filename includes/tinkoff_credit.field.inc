<?php

/**
 * Implements hook_field_info().
 */
function tinkoff_credit_field_info() {
  return array(
    'tinkoff_credit_promocodes_list' => array(
      'label'             => t('Tinkoff credit'),
      'description'       => t("This field stores promocede values from a list"),
      'default_widget'    => 'tinkoff_credit_button',
      'default_formatter' => 'tinkoff_credit_default',
    ),
  );
}

/**
 * Implements hook_field_validate().
 *
 * Possible error codes:
 * - 'tinkoff_credit_illegal_value': The value is not part of the list of
 * allowed values.
 */
function tinkoff_credit_field_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors) {
  $promocodes = tinkoff_credit_get_promocodes();
  foreach ($items as $delta => $item) {
    if (!empty($item['value'])) {
      if (!empty($promocodes) && !isset($promocodes[$item['value']])) {
        $errors[$field['field_name']][$langcode][$delta][] = array(
          'error'   => 'tinkoff_credit_illegal_value',
          'message' => t('%name: illegal value.', array('%name' => $instance['label'])),
        );
      }
    }
  }
}

/**
 * Implements hook_field_is_empty().
 */
function tinkoff_credit_field_is_empty($item, $field) {
  if (empty($item['value']) && (string) $item['value'] !== '0') {
    return TRUE;
  }
  return FALSE;
}

/**
 * Implements hook_field_formatter_info().
 */
function tinkoff_credit_field_formatter_info() {
  return array(
    'tinkoff_credit_default' => array(
      'label'       => t('Default'),
      'field types' => array('tinkoff_credit_promocodes_list'),
    ),
  );
}

/**
 * Implements hook_field_formatter_view().
 */
function tinkoff_credit_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  if ($display['type'] == 'tinkoff_credit_default' && !empty($items)) {
    $promocodes = tinkoff_credit_get_promocodes();
    foreach ($items as $delta => $item) {
      if (!empty($promocodes[$item['value']])) {
        $type = $promocodes[$item['value']]['type'];
        $button_types = tinkoff_credit_button_types();
        $element[$delta] = array(
          '#type' => 'button',
          '#value' => $button_types[$type],
          '#attributes' => array(
            'id' => 'buy-in-credit-button',
            'data-product-id' => $entity->product_id,
          ),
        );
        $element['#attached']['js'][] = drupal_get_path('module', 'tinkoff_credit') . '/js/tinkoff_credit.js';
      }
    }
  }
  return $element;
}

/**
 * Implements hook_field_widget_info().
 */
function tinkoff_credit_field_widget_info() {
  return [
    'tinkoff_credit_button' => [
      'label' => t('Buy on credit button'),
      'field types' => ['tinkoff_credit_promocodes_list'],
    ],
  ];
}

/**
 * Implements hook_field_widget_form().
 */
function tinkoff_credit_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $value = !empty($items[$delta]['value']) ? $items[$delta]['value'] : 0;
  $widget = $element;
  $widget['#delta'] = $delta;
  if ($instance['widget']['type'] == 'tinkoff_credit_button') {
    $query = db_select('tinkoff_credit_promocodes', 'tcp');
    $query->fields('tcp', ['id', 'promocode_name']);
    $result = $query->execute();
    $options = array();
    foreach ($result as $row) {
      $options[$row->id] = $row->promocode_name;
    }
    $widget += array(
      '#type' => 'radios',
      '#default_value' => $value,
      '#options' => $options,
    );
  }
  $element['value'] = $widget;
  return $element;
}

function tinkoff_credit_field_widget_error($element, $error, $form, &$form_state) {
  switch ($error['error']) {
    case 'tinkoff_credit_illegal_value':
      form_error($element, $error['message']);
      break;
  }
}

function tinkoff_credit_form_field_ui_field_edit_form_alter(&$form, &$form_state, $form_id) {
  if ($form['#field']['type'] == 'tinkoff_credit_promocodes_list') {
    $form['field']['cardinality']['#options'] = array(1);
  }
}
